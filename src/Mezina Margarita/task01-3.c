#include <stdio.h>
int main()
{
	printf("Enter line: ");
	char s[256];
	fgets(s, 256, stdin);
	int i, sum = 0, k = 0;
	for (i = 0; i < strlen(s); ++i)
	{
		if (s[i] >= '0' && s[i] <= '9')
			k = k * 10 + s[i]-'0';
		else if (k != 0)
		{
			sum = sum + k;
			k = 0;
		}
	}
	printf("Sum: %i", sum);
	return 0;
}